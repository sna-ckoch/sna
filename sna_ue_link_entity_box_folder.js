/**
 * Copyright (c) 2020, ScaleNorth Advisors LLC and/or its affiliates. All rights reserved.
 *
 * @author ckoch
 * @NApiVersion 2.x
 * @NScriptType UserEventScript
 * @NModuleScope SameAccount
 *
 * Script brief description:
 * Creates box integration custom record to link transaction with box folder associated with entity
 *
 * Revision History:
 *
 * Date              Issue/Case         Author          Issue Fix Summary
 * =============================================================================================
 * 2020/Feb/12                          ckoch           Initial version
 *
 */
define(['N/search', 'N/record'], function (search, record) {

    function afterSubmit(context) {
        if (context.type != context.UserEventType.CREATE && context.type != context.UserEventType.EDIT) return;

        var rec = context.newRecord;

        // check for existing linkage record for txn, exit if exists
        var existingFolder = getBoxFolderId(rec.type, rec.id);
        if (existingFolder != null) return;

        // get entity, exit if empty
        var entityId = context.newRecord.getValue({ fieldId: 'entity' }) || null;
        if (entityId == null) return;

        // get stage of entity - lead, prospect, customer
        var stage = getStage(entityId);
        if (stage == null) return;

        // get box folder linked with entity, exit if none
        var folderId = getBoxFolderId(stage, entityId);
        if (folderId == null) return;

        try {
            record.create({
                type: 'customrecord_box_record_folder'
            }).setValue({
                fieldId: 'custrecord_netsuite_record_type',
                value: rec.type
            }).setValue({
                fieldId: 'custrecord_ns_record_id',
                value: parseInt(rec.id)
            }).setValue({
                fieldId: 'custrecord_box_record_folder_id',
                value: folderId
            }).save();
        } catch (e) {
            log.error({
                title: 'LINK_BOX_FOLDER',
                details: JSON.stringify({
                    recType: rec.type,
                    recId: rec.id,
                    folderId: folderId,
                    e: e
                })
            });
        }
    }

    function getBoxFolderId(recType, recId) {
        var output = null;

        search.create({
            type: 'customrecord_box_record_folder',
            filters: [
                ['isinactive', 'is', 'F'],
                'and',
                ['custrecord_netsuite_record_type', 'is', recType],
                'and',
                ['custrecord_ns_record_id', 'is', recId]
            ],
            columns: [
                'custrecord_netsuite_record_type',
                'custrecord_ns_record_id',
                'custrecord_box_record_folder_id'
            ]
        }).run().each(function (result) {
            output = result.getValue({name: 'custrecord_box_record_folder_id'}) || null;
            return false;
        });

        return output;
    }

    function getStage(entityId) {
        var output = null;

        try {
            output = search.lookupFields({
                type: 'customer',
                id: entityId,
                columns: ['stage']
            }).stage[0].value.toLowerCase();
        } catch (e) {
            log.debug({
                title: 'ENTITY_STAGE_LOOKUP',
                details: JSON.stringify({
                    entityId: entityId,
                    e: e
                })
            });
        }

        return output;
    }

    return {
        afterSubmit: afterSubmit
    }

});