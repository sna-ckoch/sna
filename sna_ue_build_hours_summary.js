/**
 * Copyright (c) 2020, ScaleNorth Advisors LLC and/or its affiliates. All rights reserved.
 *
 * @author ckoch
 * @NApiVersion 2.x
 * @NScriptType UserEventScript
 * @NModuleScope SameAccount
 *
 * Script brief description:
 * Creates json object of hours summary and passes to record on print event
 *
 * Revision History:
 *
 * Date              Issue/Case         Author          Issue Fix Summary
 * =============================================================================================
 * 2020/01/28                           ckoch           Initial version
 *
 */
define(['N/search', 'N/record', 'N/format', 'N/runtime'], function (search, record, format, runtime) {

    function beforeLoad(context) {
        if (context.type != context.UserEventType.PRINT) return;
        if (context.newRecord.id == null) return;

        var rec = record.load({ type: context.newRecord.type, id: context.newRecord.id });

        var summary = buildHoursSummary(rec);

        log.debug({ title: 'SUMMARY_JSON', details: JSON.stringify(summary) });

        rec.setValue({
            fieldId: 'custbody_sna_hours_summary_json',
            value: JSON.stringify(summary)
        });
    }

    function buildHoursSummary(rec) {
        var output = [];

        var time = [], timeIds = [];

        for (var i = 0; i < rec.getLineCount({ sublistId: 'time' }); i++) {
            var apply = (rec.getSublistValue({ sublistId: 'time', fieldId: 'apply', line: i }) == true);

            if (apply) {
                var timeId = rec.getSublistValue({ sublistId: 'time', fieldId: 'doc', line: i });

                timeIds.push(timeId);

                time.push({
                    id: timeId,
                    qty: fixnum(rec.getSublistValue({ sublistId: 'time', fieldId: 'qty', line: i })),
                    rate: fixnum(rec.getSublistValue({ sublistId: 'time', fieldId: 'rate', line: i })),
                    amount: fixnum(rec.getSublistValue({ sublistId: 'time', fieldId: 'amount', line: i }))
                });
            }
        }

        log.debug({ title: 'time', details: JSON.stringify(time) });
        log.debug({ title: 'timeIds', details: JSON.stringify(timeIds) });

        if (timeIds.length == 0) return;

        var taskInfo = getTaskInfo(timeIds);

        log.debug({ title: 'taskInfo', details: JSON.stringify(taskInfo) });

        for (var i in time) {
            var timeId = time[i].id;
            var qty = time[i].qty;
            var amount = time[i].amount;
            var topLevel = 'Uncategorized';
            var parent = 'Uncategorized';

            if (taskInfo.hasOwnProperty(timeId)) {
                topLevel = taskInfo[timeId].topLevel;
                parent = taskInfo[timeId].parent;
            }

            var parentIndex = -1;

            for (var i in output) {
                if (topLevel == output[i].topLevel) {
                    parentIndex = i;
                }
            }

            if (parentIndex == -1) {
                parentIndex = output.length;

                output.push({
                    name: topLevel,
                    qty: qty,
                    amount: amount,
                    detail: []
                });
            } else {
                output[parentIndex].qty += qty;
                output[parentIndex].amount += amount;
            }

            var childIndex = -1;

            for (var i in output[parentIndex].detail) {
                if (parent == output[parentIndex].detail[i].parent) {
                    childIndex = i;
                }
            }

            if (childIndex == -1) {
                childIndex = output[parentIndex].detail.length;

                output[parentIndex].detail.push({
                    name: parent,
                    qty: qty,
                    amount: amount
                });
            } else {
                output[parentIndex].detail[childIndex].qty += qty;
                output[parentIndex].detail[childIndex].amount += amount;
            }
        }

        return output;
    }

    function getTaskInfo(timeIds) {
        var output = {};

        var colTopLevel = search.createColumn({
            name: "formulatext",
            formula: "SUBSTR({title}, 1, INSTR({title}, ' : '))"
        });

        var colTimeId = search.createColumn({
            name: 'internalid',
            join: 'time'
        });

        search.create({
            type: search.Type.PROJECT_TASK,
            filters: [
                ['time.internalid', 'anyof', timeIds]
            ],
            columns: [colTopLevel, 'parent', colTimeId]
        }).run().each(function (result) {
            output[result.getValue(colTimeId)] = {
                topLevel: result.getValue(colTopLevel),
                parent: result.getText({ name: 'parent' })
            };

            return true;
        });

        return output;
    }

    function fixnum(what) {
        return (isNaN(parseFloat(what)) ? 0.00 : parseFloat(what));
    }

    return {
        beforeLoad: beforeLoad
    }

});