/**
 * Copyright (c) 2020, ScaleNorth Advisors LLC and/or its affiliates. All rights reserved.
 *
 * @author ckoch
 * @NApiVersion 2.x
 * @NScriptType UserEventScript
 * @NModuleScope SameAccount
 *
 * Script brief description:
 * When a record is linked to lead/prospect/customer, auto create other linkages
 * ex: linkage with Lead created --- auto-link Prospect and Customer
 *
 * Revision History:
 *
 * Date              Issue/Case         Author          Issue Fix Summary
 * =============================================================================================
 * 2020/Feb/12                          ckoch           Initial version
 *
 */
define(['N/search', 'N/record'], function (search, record) {

    function afterSubmit(context) {
        if (context.type != context.UserEventType.CREATE) return;

        var rec = context.newRecord;

        var linkRecType = rec.getValue({fieldId: 'custrecord_netsuite_record_type'}) || null;
        var linkRecId = rec.getValue({fieldId: 'custrecord_ns_record_id'}) || null;
        var linkFolderId = rec.getValue({fieldId: 'custrecord_box_record_folder_id'}) || null;

        if (linkRecType == null || linkRecId == null || linkFolderId == null) return;
        
        var createLinks = ['lead','prospect','customer'];

        if (createLinks.indexOf(linkRecType) > -1) {
            for (var i in createLinks) {
                if (createLinks[i] != linkRecType) {
                    var existingFolder = getBoxFolderId(createLinks[i], linkRecId);

                    if (existingFolder == null) {
                        try {
                            record.create({
                                type: 'customrecord_box_record_folder'
                            }).setValue({
                                fieldId: 'custrecord_netsuite_record_type',
                                value: linkRecType
                            }).setValue({
                                fieldId: 'custrecord_ns_record_id',
                                value: linkRecId
                            }).setValue({
                                fieldId: 'custrecord_box_record_folder_id',
                                value: linkFolderId
                            }).save();
                        } catch (e) {
                            log.error({
                                title: 'PRELINK_BOX_FOLDER',
                                details: JSON.stringify({
                                    linkRecType: linkRecType,
                                    linkRecId: linkRecId,
                                    linkFolderId: linkFolderId,
                                    e: e
                                })
                            });
                        }
                    }
                }
            }
        }
    }

    function getBoxFolderId(recType, recId) {
        var output = null;

        search.create({
            type: 'customrecord_box_record_folder',
            filters: [
                ['isinactive', 'is', 'F'],
                'and',
                ['custrecord_netsuite_record_type', 'is', recType],
                'and',
                ['custrecord_ns_record_id', 'is', recId]
            ],
            columns: [
                'custrecord_netsuite_record_type',
                'custrecord_ns_record_id',
                'custrecord_box_record_folder_id'
            ]
        }).run().each(function (result) {
            output = result.getValue({name: 'custrecord_box_record_folder_id'}) || null;
            return false;
        });

        return output;
    }

    return {
        afterSubmit: afterSubmit
    }

});