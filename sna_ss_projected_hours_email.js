/**
 * @NApiVersion 2.0
 * @NScriptType ScheduledScript
 */
define(['N/search','N/email'], function (search,email) {

    function execute(context) {
        var timeData = getData();
        if (timeData.length == 0) return;

        var today = new Date();
        var yearStart = new Date(today.getFullYear(), 0, 1);
        var yearEnd = new Date(today.getFullYear(), 11, 31);
        var daysLeft = Math.ceil(Math.abs(yearEnd - today) / (1000 * 60 * 60 * 24));
      	var currentYear = new Date().getFullYear();

        var html = '<html><head>' + "\r\n";
		html += '<title></title>' + "\r\n";
		html += '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />' + "\r\n";
		html += '<meta name="viewport" content="width=device-width, initial-scale=1"><meta http-equiv="X-UA-Compatible" content="IE=edge" />' + "\r\n";
		html += '<style type="text/css">' + "\r\n";
		html += '    /* CLIENT-SPECIFIC STYLES */' + "\r\n";
		html += '    body, table, td, a { -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; }' + "\r\n";
		html += '    table, td { mso-table-lspace: 0pt; mso-table-rspace: 0pt; }' + "\r\n";
		html += '    img { -ms-interpolation-mode: bicubic; }' + "\r\n";
		html += '    /* RESET STYLES */' + "\r\n";
		html += '    img { border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; }' + "\r\n";
		html += '    table { border-collapse: collapse !important; }' + "\r\n";
		html += '    body { height: 100% !important; margin: 0 !important; padding: 0 !important; width: 100% !important; }' + "\r\n";
		html += '    /* iOS BLUE LINKS */' + "\r\n";
		html += '    a[x-apple-data-detectors] {' + "\r\n";
		html += '        color: inherit !important;' + "\r\n";
		html += '        text-decoration: none !important;' + "\r\n";
		html += '        font-size: inherit !important;' + "\r\n";
		html += '        font-family: inherit !important;' + "\r\n";
		html += '        font-weight: inherit !important;' + "\r\n";
		html += '        line-height: inherit !important;' + "\r\n";
		html += '    }' + "\r\n";
		html += '    /* MOBILE STYLES */' + "\r\n";
		html += '    @media screen and (max-width:600px){' + "\r\n";
		html += '        h1 {' + "\r\n";
		html += '            font-size: 32px !important;' + "\r\n";
		html += '            line-height: 32px !important;' + "\r\n";
		html += '        }' + "\r\n";
		html += '    }' + "\r\n";
		html += '    /* ANDROID CENTER FIX */' + "\r\n";
		html += '    div[style*="margin: 16px 0;"] { margin: 0 !important; }' + "\r\n";
		html += '</style>' + "\r\n";
		html += '</head><body style="background-color: #f4f4f4; margin: 0 !important; padding: 0 !important;"><!-- HIDDEN PREHEADER TEXT -->' + "\r\n";
		html += '<div style="display: none; font-size: 1px; color: #fefefe; line-height: 1px; font-family: Helvetica, Arial, sans-serif; ' + "\r\n";
      	html += 'max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden;">SNA Weekly Chargeable and Product Hours Report</div>' + "\r\n";
		html += '<table border="0" cellpadding="0" cellspacing="0" width="100%"><!-- LOGO -->' + "\r\n";
		html += '<tbody>' + "\r\n";
		html += '	<tr>' + "\r\n";
		html += '	<td align="center" bgcolor="#81A7BC"><!--[if (gte mso 9)|(IE)]>' + "\r\n";
		html += '            <table align="center" border="0" cellspacing="0" cellpadding="0" width="1024px">' + "\r\n";
		html += '            <tr>' + "\r\n";
		html += '            <td align="center" valign="top" width="1024px">' + "\r\n";
		html += '            <![endif]--><!--[if (gte mso 9)|(IE)]>' + "\r\n";
		html += '            </td>' + "\r\n";
		html += '            </tr>' + "\r\n";
		html += '            </table>' + "\r\n";
		html += '            <![endif]--></td>' + "\r\n";
		html += '	</tr>' + "\r\n";
		html += '	<!-- HERO -->' + "\r\n";
		html += '	<tr>' + "\r\n";
		html += '	<td align="center" bgcolor="#81A7BC" style="padding: 0px 10px 0px 10px;"><!--[if (gte mso 9)|(IE)]>' + "\r\n";
		html += '            <table align="center" border="0" cellspacing="0" cellpadding="0" width="1024px">' + "\r\n";
		html += '            <tr>' + "\r\n";
		html += '            <td align="center" valign="top" width="1024px">' + "\r\n";
		html += '            <![endif]-->' + "\r\n";
		html += '	<table border="0" cellpadding="0" cellspacing="0" style="max-width: 1024px;" width="100%">' + "\r\n";
		html += '	<tbody>' + "\r\n";
		html += '		<tr>' + "\r\n";
		html += '		<td align="center" style="padding: 40px 10px 40px 10px;" valign="top">&nbsp;</td>' + "\r\n";
		html += '		</tr>' + "\r\n";
		html += '		<tr>' + "\r\n";
		html += '		<td align="center" bgcolor="#ffffff" style="padding: 40px 20px 20px 20px; border-radius: 4px 4px 0px 0px; color: #111111; ' + "\r\n";
      	html += 'font-family: Helvetica, Arial, sans-serif; font-size: 48px; font-weight: 400; letter-spacing: 4px; line-height: 48px;" valign="top">' + "\r\n";
		html += '       <a href="https://scalenorthadvisors.com" target="_blank">' + "\r\n";
		html += '       <img alt="SNALogo" border="5px solid #fff" height="50" ' + "\r\n";
      	html += 'src="https://scalenorthadvisors.com/wp-content/uploads/2019/12/ScaleNorth_Left-Justified_web.jpg" style="display: block; width: 278px; ' + "\r\n";
      	html += 'max-width: 278px; min-width: 278px; font-family: Helvetica, Arial, sans-serif; color: #ffffff; font-size: 18px;" width="278" /> </a></td>' + "\r\n";
		html += '		</tr>' + "\r\n";
		html += '		<tr>' + "\r\n";
		html += '		<td align="center" bgcolor="#ffffff" style="padding: 20px 20px 20px 20px; color: #111111; font-family: Helvetica, Arial, sans-serif; ' + "\r\n";
      	html += 'font-size: 20px; font-weight: 400; letter-spacing: 4px; line-height: 48px;" valign="top">' + "\r\n";
		html += '		SNA Weekly Chargeable Hours ' + currentYear;
		html += '		</td>' + "\r\n";
		html += '		</tr>' + "\r\n";
		html += '	</tbody>' + "\r\n";
		html += '	</table>' + "\r\n";
		html += '	<!--[if (gte mso 9)|(IE)]>' + "\r\n";
		html += '            </td>' + "\r\n";
		html += '            </tr>' + "\r\n";
		html += '            </table>' + "\r\n";
		html += '            <![endif]--></td>' + "\r\n";
		html += '	</tr>' + "\r\n";
		html += '	<!-- COPY BLOCK -->' + "\r\n";
		html += '	<tr>' + "\r\n";
		html += '	<td align="center" bgcolor="#f4f4f4" style="padding: 0px 10px 0px 10px;"><!--[if (gte mso 9)|(IE)]>' + "\r\n";
		html += '            <table align="center" border="0" cellspacing="0" cellpadding="0" width="1024px">' + "\r\n";
		html += '            <tr>' + "\r\n";
		html += '            <td align="center" valign="top" width="1024px">' + "\r\n";
		html += '            <![endif]-->' + "\r\n";
		html += '	<table border="0" cellpadding="0" cellspacing="0" style="max-width: 1024px;" width="100%"><!-- COPY -->' + "\r\n";
		html += '	<tbody>' + "\r\n";
		html += '		<tr>' + "\r\n";
		html += '		<td align="center" bgcolor="#ffffff" style="padding: 5px 30px 10px 20px; color: #666666; font-family: Helvetica, Arial, sans-serif; ' + "\r\n";
      	html += 'font-size: 14px; font-weight: 400; line-height: 20px;">' + "\r\n";
            
        html += '<table><tr>' + "\r\n";
      	html += '<th align="left" style="padding: 10px">Employee</th>';
      	html += '<th align="right" style="padding: 10px">Product & Sales<br>Hrs YTD</th>';
      	html += '<th align="right" style="padding: 10px">Client Hrs<br>2WKS</th>';
      	html += '<th align="right" style="padding: 10px">Client Hrs<br>PREV WK</th>';
      	html += '<th align="right" style="padding: 10px">Client Hrs<br>YTD</th>';
      	html += '<th align="right" style="padding: 10px">Projected<br>Prod & Sales<br>Hrs</th>';
      	html += '<th align="right" style="padding: 10px">Projected<br>Client Hrs</th>';
      	html += '<th align="right" style="padding: 10px">Projected<br>Sum Hrs</th>';
      	html += '</tr>' + "\r\n" ;

        for (var i in timeData) {
            var internalTime = timeData[i].internalHours;
            var ytdTime = timeData[i].allYtd;
            var startDate = Math.max(yearStart, timeData[i].hireDate);
            var daysWorked = Math.ceil(Math.abs(today - startDate) / (1000 * 60 * 60 * 24));
            if (daysWorked == 0) daysWorked = 1;

            var avgInternalHours = internalTime / daysWorked;
            var projectedInternal = internalTime + (avgInternalHours * daysLeft);

            var avgAllHours = ytdTime / daysWorked;
            var projectedAll = ytdTime + (avgAllHours * daysLeft);
			html += "\r\n";
            html += '<tr style="background-color:';
          	if(i % 2 == 0)
            	{ html+='#d3d3d3'; }
          	else
              	{ html+='#fff';}
          	html +='">';
            html += '<td align="left" style="padding: 10px">' + timeData[i].employee.text + '</td>';
            html += '<td align="right" style="padding: 10px">' + internalTime.toFixed(2) + '</td>';
            html += '<td align="right" style="padding: 10px">' + timeData[i].allTwoWk.toFixed(2) + '</td>';
            html += '<td align="right" style="padding: 10px">' + timeData[i].allPrevWk.toFixed(2) + '</td>';
            html += '<td align="right" style="padding: 10px">' + ytdTime.toFixed(2) + '</td>';
            html += '<td align="right" style="padding: 10px">' + projectedInternal.toFixed(2) + '</td>';
            html += '<td align="right" style="padding: 10px">' + projectedAll.toFixed(2) + '</td>';
          	var sumTime = parseFloat(projectedInternal) + parseFloat(projectedAll);
          	html += '<td align="right" style="padding: 10px">' + sumTime.toFixed(2) + '</td>';
            html += '</tr>' + "\r\n";
        }

        html += '</table>' + "\r\n";
      	html += '		</td>' + "\r\n";
		html += '		</tr>' + "\r\n";
		html += '		<!-- BULLETPROOF BUTTON -->' + "\r\n";
		html += '		<tr>' + "\r\n";
		html += '		<td align="left" bgcolor="#ffffff">' + "\r\n";
		html += '		<table border="0" cellpadding="0" cellspacing="0" width="100%">' + "\r\n";
		html += '		<tbody>' + "\r\n";
		html += '			<tr>' + "\r\n";
		html += '			<td align="center" bgcolor="#ffffff" style="padding: 20px 30px 60px 30px;">' + "\r\n";
		html += '			<table border="0" cellpadding="0" cellspacing="0">' + "\r\n";
		html += '			<tbody>' + "\r\n";
		html += '				<tr>' + "\r\n";
		html += '				<td align="center" bgcolor="#81A7BC" style="border-radius: 3px;"><a href="https://scalenorthadvisors.com" ' + "\r\n";
      	html += 'style="font-size: 20px; font-family: Helvetica, Arial, sans-serif; color: #ffffff; text-decoration: none; color: #ffffff; ' + "\r\n";
      	html += 'text-decoration: none; padding: 15px 25px; border-radius: 2px; border: 1px solid #81A7BC; display: inline-block;" target="_blank">' + "\r\n";
      	html += 'Visit our web site</a></td>' + "\r\n";
		html += '				</tr>' + "\r\n";
		html += '			</tbody>' + "\r\n";
		html += '			</table>' + "\r\n";
		html += '			</td>' + "\r\n";
		html += '			</tr>' + "\r\n";
		html += '		</tbody>' + "\r\n";
		html += '		</table>' + "\r\n";
		html += '		</td>' + "\r\n";
		html += '		</tr>' + "\r\n";
		html += '	</tbody>' + "\r\n";
		html += '	</table>' + "\r\n";
		html += '	<!--[if (gte mso 9)|(IE)]>' + "\r\n";
		html += '            </td>' + "\r\n";
		html += '            </tr>' + "\r\n";
		html += '            </table>' + "\r\n";
		html += '            <![endif]--></td>' + "\r\n";
		html += '	</tr>' + "\r\n";
		html += '	<!-- COPY CALLOUT -->' + "\r\n";
		html += '	<tr>' + "\r\n";
		html += '	<td align="center" bgcolor="#f4f4f4" style="padding: 0px 10px 0px 10px;"><!--[if (gte mso 9)|(IE)]>' + "\r\n";
		html += '            <table align="center" border="0" cellspacing="0" cellpadding="0" width="1024px">' + "\r\n";
		html += '            <tr>' + "\r\n";
		html += '            <td align="center" valign="top" width="1024px">' + "\r\n";
		html += '            <![endif]-->' + "\r\n";
		html += '	<table border="0" cellpadding="0" cellspacing="0" style="max-width: 1024px;" width="100%"><!-- HEADLINE -->' + "\r\n";
		html += '	<tbody>' + "\r\n";
		html += '		<tr>' + "\r\n";
		html += '		<td align="left" bgcolor="#111111" style="padding: 40px 30px 20px 30px; color: #ffffff; font-family: Helvetica, Arial, sans-serif; ' + "\r\n";
      	html += 'font-size: 18px; font-weight: 400; line-height: 25px;">' + "\r\n";
		html += '		<h2 style="font-size: 24px; font-weight: 400; margin: 0;">Please note</h2>' + "\r\n";
		html += '		</td>' + "\r\n";
		html += '		</tr>' + "\r\n";
		html += '		<!-- COPY -->' + "\r\n";
		html += '		<tr>' + "\r\n";
		html += '		<td align="left" bgcolor="#111111" style="padding: 0px 30px 20px 30px; color: #666666; font-family: Helvetica, Arial, sans-serif; ' + "\r\n";
      	html += 'font-size: 18px; font-weight: 400; line-height: 25px;">' + "\r\n";
		html += '		<p style="margin: 0;">If you have any questions or comments, please email <a href="mailto:cemory@scalenorthadvisors.com" ' + "\r\n";
      	html += 'style="color: #ec6d64;">cemory@scalenorthadvisors.com</a></p>' + "\r\n";
		html += '		</td>' + "\r\n";
		html += '		</tr>' + "\r\n";
		html += '		<!-- COPY -->' + "\r\n";
		html += '	</tbody>' + "\r\n";
		html += '	</table>' + "\r\n";
		html += '	<!--[if (gte mso 9)|(IE)]>' + "\r\n";
		html += '            </td>' + "\r\n";
		html += '            </tr>' + "\r\n";
		html += '            </table>' + "\r\n";
		html += '            <![endif]--></td>' + "\r\n";
		html += '	</tr>' + "\r\n";
		html += '	<!-- SUPPORT CALLOUT -->' + "\r\n";
		html += '	<!-- FOOTER -->' + "\r\n";
		html += '	<tr>' + "\r\n";
		html += '	<td align="center" bgcolor="#f4f4f4" style="padding: 0px 10px 0px 10px;"><!--[if (gte mso 9)|(IE)]>' + "\r\n";
		html += '            <table align="center" border="0" cellspacing="0" cellpadding="0" width="1024px">' + "\r\n";
		html += '            <tr>' + "\r\n";
		html += '            <td align="center" valign="top" width="1024px">' + "\r\n";
		html += '            <![endif]-->' + "\r\n";
		html += '	<table border="0" cellpadding="0" cellspacing="0" style="max-width: 1024px;" width="100%"><!-- NAVIGATION -->' + "\r\n";
		html += '	<tbody>' + "\r\n";
		html += '		<!-- ADDRESS -->' + "\r\n";
		html += '		<tr>' + "\r\n";
		html += '		<td align="left" bgcolor="#f4f4f4" style="padding: 0px 30px 30px 30px; color: #666666; font-family: Helvetica, Arial, sans-serif; ' + "\r\n";
      	html += 'font-size: 14px; font-weight: 400; line-height: 18px;">' + "\r\n";
		html += '		<p style="margin: 0;">&nbsp;</p>' + "\r\n";
		html += '		</td>' + "\r\n";
		html += '		</tr>' + "\r\n";
		html += '        <tr>' + "\r\n";
		html += '		<td align="center" bgcolor="#f4f4f4" style="padding: 0px 30px 0px 30px; color: #666666; font-family: Helvetica, Arial, sans-serif; ' + "\r\n";
      	html += 'font-size: 14px; font-weight: 400; line-height: 18px;">' + "\r\n";
		html += '		<p style="margin: 0;">ScaleNorth Advisors, LLC</p>' + "\r\n";
		html += '		</td>' + "\r\n";
		html += '		</tr>' + "\r\n";
		html += '        <tr>' + "\r\n";
		html += '		<td align="center" bgcolor="#f4f4f4" style="padding: 0px 30px 0px 30px; color: #666666; font-family: Helvetica, Arial, sans-serif; ' + "\r\n";
      	html += 'font-size: 14px; font-weight: 400; line-height: 18px;">' + "\r\n";
		html += '		<p style="margin: 0;">1651 E. 4th St Ste 125 - Santa Ana, CA - 92701</p>' + "\r\n";
		html += '		</td>' + "\r\n";
		html += '		</tr>' + "\r\n";
		html += '        <tr>' + "\r\n";
		html += '		<td align="center" bgcolor="#f4f4f4" style="padding: 0px 30px 0px 30px; color: #666666; font-family: Helvetica, Arial, sans-serif; ' + "\r\n";
      	html += 'font-size: 14px; font-weight: 400; line-height: 18px;">' + "\r\n";
		html += '		<p style="margin: 0;">(714) 705-6075</p>' + "\r\n";
		html += '		</td>' + "\r\n";
		html += '		</tr>' + "\r\n";
		html += '	</tbody>' + "\r\n";
		html += '	</table>' + "\r\n";
		html += '	<!--[if (gte mso 9)|(IE)]>' + "\r\n";
		html += '            </td>' + "\r\n";
		html += '            </tr>' + "\r\n";
		html += '            </table>' + "\r\n";
		html += '            <![endif]--></td>' + "\r\n";
		html += '	</tr>' + "\r\n";
		html += '</tbody>' + "\r\n";
		html += '</table>' + "\r\n";
		html += '</body></html>' + "\r\n";

        email.send({
            author: '9074',
            recipients: ['sna_all_staff@scalenorthadvisors.com','cemory@scalenorth.com'],
            subject: 'SNA Weekly Chargeable and Product Hours Report',
            body: html
        });
    }

    function getData() {
        var output = [];

        var columns = [
            search.createColumn({
                name: 'employee',
                summary: 'GROUP'
            }),
            search.createColumn({
                name: 'hiredate',
                join: 'employee',
                summary: 'GROUP'
            }),
            search.createColumn({
                name: 'formulanumeric',
                summary: 'SUM',
                formula: "CASE WHEN ({job.internalid}='9073' OR {job.internalid}='13855') THEN {durationdecimal} ELSE 0 END"
            }),
            search.createColumn({
                name: 'formulanumeric',
                summary: 'SUM',
                formula: "CASE WHEN ({exempt}='F') THEN (CASE WHEN ((TRUNC(SYSDATE)-TRUNC({date})) > 7 AND (TRUNC(SYSDATE)-TRUNC({date})) <= 14) THEN {durationdecimal} ELSE 0 END) END"
            }),
            search.createColumn({
                name: 'formulanumeric',
                summary: 'SUM',
                formula: "CASE WHEN ({exempt}='F') THEN (CASE WHEN (((TRUNC(SYSDATE)-TRUNC({date})) <= 7) AND ((TRUNC(SYSDATE)-TRUNC({date})) >= 1)) THEN {durationdecimal} ELSE 0 END) END"
            }),
            search.createColumn({
                name: 'formulanumeric',
              	summary: 'SUM',
              	formula: "CASE WHEN ({exempt}='F') THEN {durationdecimal} ELSE 0 END"
                
            })
        ];

        search.create({
            type: 'timebill',
            filters:
                [
                    ['date', 'within', 'thisyear'],
                    'AND',
                    ['job.jobname', 'doesnotcontain', 'sna'],
                    'AND',
                    [['exempt', 'is', 'F'], 'OR', ['job.internalid', 'anyof', ['9073','13855']]],
                    'AND',
                    ['type', 'anyof', 'A'],
                    'AND',
                    ['employee.isinactive', 'is', 'F'],
                    'AND',
                    ['employee.hiredate', 'isnotempty', '']
                ],
            columns: columns
        }).run().each(function (result) {
            output.push({
                employee: {
                    id: result.getValue(columns[0]),
                    text: result.getText(columns[0])
                },
                hireDate: new Date(result.getValue(columns[1])),
                internalHours: forceFloat(result.getValue(columns[2])),
                allTwoWk: forceFloat(result.getValue(columns[3])),
                allPrevWk: forceFloat(result.getValue(columns[4])),
                allYtd: forceFloat(result.getValue(columns[5]))
            });

            return true;
        });

        return output;
    }

    function forceFloat(what) {
        return (isNaN(parseFloat(what)) ? 0.0 : parseFloat(what));
    }

    return {
        execute: execute
    }

});